pragma solidity ^0.4.2;

contract Election {
    // 후보자 구조체
    struct Candidate {
        uint id;
        string name;
        uint voteCount;
    }

    // 지갑계정에 boolean 으로 투표 결과 맵핑
    mapping(address => bool) public voters;

    // 후보자를 후보자 구조체에 맵핑
    mapping(uint => Candidate) public candidates;

    // 후보자 투표결과 저장
    uint public candidatesCount;

    // 투표 이벤트
    event votedEvent (
        uint indexed _candidateId
    );

    function Election () public {
        addCandidate("Candidate 1");
        addCandidate("Candidate 2");
    }

    function addCandidate (string _name) private {
        candidatesCount ++;
        candidates[candidatesCount] = Candidate(candidatesCount, _name, 0);
    }

    function vote (uint _candidateId) public {
        // 유권자가 투표하지 않았음을 확인
        require(!voters[msg.sender]);

        // 적합한 유권자인지 확인
        require(_candidateId > 0 && _candidateId <= candidatesCount);

        // 유권자의 투표를 기록
        voters[msg.sender] = true;

        // 후보자의 총 투표수에 1을 더함
        candidates[_candidateId].voteCount ++;

        // 투표 이벤트 실행
        votedEvent(_candidateId);
    }
}

0xDb137E5609a8b35d5318f46650113d7De8c03149